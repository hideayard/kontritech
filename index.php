<?php
function get_client_ip()
{
     $ipaddress = '';
     if (getenv('HTTP_CLIENT_IP'))
         $ipaddress = getenv('HTTP_CLIENT_IP');
     else if(getenv('HTTP_X_FORWARDED_FOR'))
         $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
     else if(getenv('HTTP_X_FORWARDED'))
         $ipaddress = getenv('HTTP_X_FORWARDED');
     else if(getenv('HTTP_FORWARDED_FOR'))
         $ipaddress = getenv('HTTP_FORWARDED_FOR');
     else if(getenv('HTTP_FORWARDED'))
         $ipaddress = getenv('HTTP_FORWARDED');
     else if(getenv('REMOTE_ADDR'))
         $ipaddress = getenv('REMOTE_ADDR');
     else
         $ipaddress = 'UNKNOWN';

     return $ipaddress;
}

$filename = 'counter.txt';	//mendefinisikan nama file untuk menyimpan counter
 
function counter(){		//function counter
	global $filename;	//set global variabel $filename
 
	if(file_exists($filename)){		//jika file counter.txt ada
		$value = file_get_contents($filename);	//set value = nilai di notepad
	}else{		//jika file counter.txt tidak ada maka akan membuat file counter.txt
		$value = 0;		//kemudian set value = 0
	}
 
	file_put_contents($filename, ++$value);		//menuliskan kedalam file counter.txt value+1
}
 
counter();	//menjalankan function counter
    $ip = get_client_ip();
    $tgl = (new \DateTime())->format('Y-m-d H:i:s');
// echo 'Total pengunjung: '.file_get_contents($filename);	//menampilkan counter di website
$myfile = fopen("ip.txt", "a") or die("Unable to open file!");
    $txt = $ip.";".$tgl;
    fwrite($myfile, "\n". $txt);
    fclose($myfile);
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Always force latest IE rendering engine -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Meta Keyword -->
        <meta name="keywords" content="one page, business template, single page, onepage, responsive, parallax, creative, business, html5, css3, css3 animation">
        <!-- meta character set -->
        <meta charset="utf-8">

        <!-- Site Title -->
        <title>kontritech</title>
        
        <!--
        Google Fonts
        ============================================= -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet" type="text/css">
		
        <!--
        CSS
        ============================================= -->
        <!-- Fontawesome -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Fancybox -->
        <link rel="stylesheet" href="css/jquery.fancybox.css">
        <!-- owl carousel -->
        <link rel="stylesheet" href="css/owl.carousel.css">
        <!-- Animate -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="css/main.css">
        <!-- Main Responsive -->
        <link rel="stylesheet" href="css/responsive.css">
		
		
		<!-- Modernizer Script for old Browsers -->
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
		
    </head>
	
    <body>

        <!--
        Fixed Navigation
        ==================================== -->
        <header id="navigation" class="navbar-fixed-top">
            <div class="container">

                <div class="navbar-header">
                    <!-- responsive nav button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- /responsive nav button -->

                    <!-- logo -->
                    <h1 class="navbar-brand">
                        <a href="#home">
                            <img src="img/logo_150_white.png" alt="Kontri Logo">
                        </a>
                    </h1>
                    <!-- /logo -->

                    </div>

                    <!-- main nav -->
                    <nav class="collapse navigation navbar-collapse navbar-right" role="navigation">
                        <ul id="nav" class="nav navbar-nav">
                            <!-- <li class="current"><a href="#home">Home</a></li> -->
                            
                            <li><a href="#service">Service</a></li>
                            <li><a href="#portfolio">portfolio</a></li>
                            <li><a href="#about">About</a></li>
                            <!-- <li><a href="#pricing">Pricing</a></li> -->
                            <li><a href="#contact">Contact</a></li>
                        </ul>
                    </nav>
                    <!-- /main nav -->
                </div>

            </div>
        </header>
        <!--
        End Fixed Navigation
        ==================================== -->


        <!--
        Home Slider
        ==================================== -->
        <section id="home">     
            <div id="home-carousel" class="carousel slide" data-interval="false">
                <ol class="carousel-indicators">
                    <li data-target="#home-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#home-carousel" data-slide-to="1"></li>
                    <li data-target="#home-carousel" data-slide-to="2"></li>
                </ol>
                <!--/.carousel-indicators-->

                <div class="carousel-inner">

                    <div class="item active"  style="background-image: url('img/slider/bg1.jpg')" >
                        <div class="carousel-caption">
                            <div class="animated bounceInRight">
                                <h2>Saat ini, Semua orang butuh teknologi.</h2>
                                <!-- <p>Saat ini, Semua orang butuh teknologi.</p> -->
                            </div>
                        </div>
                    </div>              
                   
                   <div class="item" style="background-image: url('img/slider/bg2.jpg')">                 
                    <div class="carousel-caption">
                        <div class="animated bounceInUp">
                            <h2>Semua pasti bisa dikerjakan</h2>
                            <!-- <p>Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur aliquet quam. Accumsan id imperdiet et, porttitor at sem. Mauris blandit aliquet elit, eget tincidunt.</p> -->
                        </div>
                    </div>
                    </div>

                    <div class="item" style="background-image: url('img/slider/bg3.jpg')">                 
                        <div class="carousel-caption">
                            <div class="animated bounceInUp">
                                <h2>Teknologi menjawab setiap kebutuhan</h2>
                                <!-- <p>Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur aliquet quam. Accumsan id imperdiet et, porttitor at sem. Mauris blandit aliquet elit, eget tincidunt.</p> -->
                            </div>
                        </div>
                        </div>
               
                </div>
                <!--/.carousel-inner-->
                <nav id="nav-arrows" class="nav-arrows hidden-xs hidden-sm visible-md visible-lg">
                    <a class="sl-prev hidden-xs" href="#home-carousel" data-slide="prev">
                        <i class="fa fa-angle-left fa-3x"></i>
                    </a>
                    <a class="sl-next" href="#home-carousel" data-slide="next">
                        <i class="fa fa-angle-right fa-3x"></i>
                    </a>
                </nav>

            </div>
        </section>
        <!--
        End #home Slider
        ========================== -->

        
        <!--
        #service
        ========================== -->
        <section id="service">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-center wow fadeInDown">
                            <h2>Services</h2>    
                            <p>kontritech is your best choice for 4.0 IT Provider</p>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-4 col-sm-12 wow fadeInLeft">
                        <div class="media">
                            <a href="#" class="pull-left">
                                <img src="img/icons/iot_icon_100.png" class="media-object" alt="Monitor">
                            </a>
                            <div class="media-body">
                                <h3>Internet of Things Platform</h3>
                                <p>Platform ini menyediakan software dan hardware yang saling terintegrasi dengan cloud server melalui jaringan internet sehingga dapat diakses dan dipantau secara real time dimana saja.</p>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-4 col-sm-12 wow fadeInDown" data-wow-delay="0.2s">
                        <div class="media">
                            <a href="#" class="pull-left">
                                <img src="img/icons/software_100.png" alt="Cog">
                            </a>
                            <div class="media-body">
                                <h3>Software & Hardware Development</h3>
                                <p>Layanan ini menyediakan custom system dari pembuatan hingga pengembangan sebuah sistem baik software maupun hardware.</p>
                            </div>
                        </div>
                        
                    </div>

                    <div class="col-md-4 col-sm-12 wow fadeInRight">
                        <div class="media">
                            <a href="#" class="pull-left">
                                <img src="img/icons/maintenance_100.png" alt="Ruler">
                            </a>
                            <div class="media-body">
                                <h3>Maintenance System and Product</h3>
                                <p> Jasa ini menyediakan perawatan, perbaikan maupun penggantian produk dari Internet of Things (ToT) Platform dan Software & Hardware Development.</p>
                            </div>
                        </div>
                    </div>

                  

                </div> <!-- end .row -->
            </div> <!-- end .container -->
        </section>
        <!--
        End #service
        ========================== -->

        <!--
        #service-bottom
        ========================== -->
        
        <section id="service-bottom">
            <div class="container">
                    <div class="mobile-device">
                       <img data-wow-delay="0.2s" class="img-responsive black  wow fadeInLeftBig" src="img/icons/iphone-black.png" alt="iPhone Black">
                        <img data-wow-delay="0.5s" class="img-responsive white  wow fadeInLeftBig" src="img/icons/iphone-white.png" alt="iPhone White">
                    </div>
                <div class="service-features wow fadeInRight">
                    <h3>WE CAN SOLVE...</h3>
                    <ul>
                        <li><strong>HUMAN RESOURCE</strong> ( Keterbatasan Sumber Daya Manusia )</li>
                        <li><strong>TIME</strong> ( Pekerjaan Tidak Dapat Termonitor Secara Real Time ) </li>
                        <li><strong>LOCATION</strong> ( Lokasi / Posisi Yang Sulit Terjangkau ) </li>
                        <li><strong>ACCURACY</strong> ( Sering Terjadi Human Error Dalam Pengukuran ) </li>
                    </ul>
                </div>
            </div>
        </section>
        <!--
        End #service-bottom
        ========================== -->


        <!--
        #Portfolio
        ========================== -->
        
        <section id="portfolio">

            <div class="section-title text-center wow fadeInDown">
                <h2>Portfolio</h2>    
                <p>Project-project yang telah kontritech kerjakan.</p>
            </div>
            
            <nav class="project-filter clearfix text-center wow fadeInLeft"  data-wow-delay="0.5s">
                <ul class="list-inline">
                    <li><a href="javascript:;" class="filter" data-filter="all">All</a></li>
                    <li><a href="javascript:;" class="filter" data-filter=".iot">IoT Platform</a></li>
                    <li><a href="javascript:;" class="filter" data-filter=".softhard">Software & Hardware Dev</a></li>
                    <li><a href="javascript:;" class="filter" data-filter=".maintenance">Maintenance System & product</a></li>
                    <!-- <li><a href="javascript:;" class="filter" data-filter=".timbangan">Timbangan</a></li> -->
                </ul>
            </nav>

            <div id="projects" class="clearfix">

                <figure class="mix portfolio-item softhard">
                    <img class="img-responsive" src="img/portfolio/jadwal_sholat_crop.jpg" alt="Portfolio Item">
                    <a href="img/portfolio/jadwal_sholat_crop.jpg" title="Jadwal Sholat" rel="portfolio" class="fancybox"><span class="plus"></span></a>
                    <figcaption class="mask">
                        <h3>Jadwal Sholat Digital</h3>
                        <span>jadwal sholat</span>
                    </figcaption>
                </figure>

                <figure class="mix portfolio-item maintenance">
                    <img class="img-responsive" src="img/portfolio/papan_skor_crop.jpg" alt="Portfolio Item">
                    <a href="img/portfolio/papan_skor_crop.jpg" title="Papan Skor" rel="portfolio" class="fancybox"><span class="plus"></span></a>
                    <figcaption class="mask">
                        <h3>Papan Skor Pertandingan</h3>
                        <span>LED Panel</span>
                    </figcaption>
                </figure>

                <figure class="mix portfolio-item softhard">
                    <img class="img-responsive" src="img/portfolio/timbangan.jpg" alt="Portfolio Item">
                    <a href="img/portfolio/timbangan.jpg" title="Timbangan Digital" rel="portfolio" class="fancybox"><span class="plus"></span></a>
                    <figcaption class="mask">
                        <h3>Timbangan Digital Pemancingan</h3>
                        <span>Timbangan Digital</span>
                    </figcaption>
                </figure>

                <figure class="mix portfolio-item iot">
                    <img class="img-responsive" src="img/portfolio/e-dermaga.jpg" alt="Portfolio Item">
                    <a href="img/portfolio/e-dermaga.jpg" title="E-Dermaga (control and monitor lamp)" rel="portfolio" class="fancybox"><span class="plus"></span></a>
                    <figcaption class="mask">
                        <h3>E Dermaga</h3>
                        <span>IoT Apps</span>
                    </figcaption>
                </figure>

                < <figure class="mix portfolio-item iot">
                    <img class="img-responsive" src="img/portfolio/counter_production.jpg" alt="Portfolio Item">
                    <a href="img/portfolio/counter_production.jpg" title="Counter Production for Factory" rel="portfolio" class="fancybox"><span class="plus"></span></a>
                    <figcaption class="mask">
                        <h3>Counter Production</h3>
                        <span>IoT Apps</span>
                    </figcaption>
                </figure>

                <!--figure class="mix portfolio-item photography">
                    <img class="img-responsive" src="img/portfolio/portfolio-1.jpg" alt="Portfolio Item">
                    <a href="img/portfolio/portfolio-1.jpg" title="Title Six" rel="portfolio" class="fancybox"><span class="plus"></span></a>
                    <figcaption class="mask">
                        <h3>Awesome Image</h3>
                        <span>Photography</span>
                    </figcaption>
                </figure>

                <figure class="mix portfolio-item web app">
                    <img class="img-responsive" src="img/portfolio/portfolio-2.jpg" alt="Portfolio Item">
                    <a href="img/portfolio/portfolio-2.jpg" title="Title Seven" rel="portfolio" class="fancybox"><span class="plus"></span></a>
                    <figcaption class="mask">
                        <h3>Awesome Image</h3>
                        <span>Photography</span>
                    </figcaption>
                </figure>

                <figure class="mix portfolio-item print web">
                    <img class="img-responsive" src="img/portfolio/portfolio-3.jpg" alt="Portfolio Item">
                    <a href="img/portfolio/portfolio-3.jpg" title="Title Eight" rel="portfolio" class="fancybox"><span class="plus"></span></a>
                    <figcaption class="mask">
                        <h3>Awesome Image</h3>
                        <span>Photography</span>
                    </figcaption>
                </figure> -->

            </div> <!-- end #projects -->

        </section>
        <!--
        End #Portfolio
        ========================== -->
<!--
        #quotes
        ========================== -->
        <section id="quotes">
            <div class="container">
                <div class="row wow zoomIn">
                    <div class="col-lg-12">
                        <div class="call-to-action text-center">
                            <p>“TECHNOLOGY FOR EVERYNEED”</p>
                            <span> - kontritech - </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--
        End #quotes
        ========================== -->
        <!--
        #about
        ========================== -->
        <section id="about">
            <div class="container">
                <div class="row">

                    <div class="section-title text-center wow fadeInUp">
                        <h2>About Us</h2>    
                        <p style="font-size: large;">Sebuah perusahaan rintisan yang bergerak dibidang teknologi sebagai penyedia Internet of Things Platform untuk mendukung dan membantu bidang usaha lain dari sektor industri maupun non industri untuk berevolusi ke industri 4.0.</p>
                    </div>
                    
                    <!-- <div class="about-us text-center">
                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.5s">
                            <img src="img/client/1.jpg" alt="Client" class="img-circle">
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInDown" data-wow-delay="0.5s">
                            <img src="img/client/1.jpg" alt="Client" class="img-circle">
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInRight" data-wow-delay="0.5s">
                            <img src="img/client/1.jpg" alt="Client" class="img-circle">
                        </div>
                        
                    </div> -->
                </div>
            </div>
        </section>
        <!--
        End #about
        ========================== -->
        

       

        




        <!--
        #subscribe
        ========================== -->
        <section id="subscribe">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 wow fadeInLeft">
                        <form action="actionContact.php" method="post" class="subscription-form">
                            <i class="fa fa-envelope-o fa-lg"></i>
                            <input type="email" name="subscribe" class="subscribe" placeholder="YOUR MAIL" required="">
                            <input type="submit" name="sbtn" value="SUBSCRIBE" id="mail-submit">
                        </form>
                    </div>

                    <!-- <div class="col-md-4 text-left wow fadeInRight">
                        <p>Subscibe for updates.</p>
                    </div> -->
                </div>
            </div>
        </section>
        <!--
        End #subscribe
        ========================== -->


        <!--
        #contact
        ========================== -->
        <section id="contact">
            <div class="container">
                <div class="row">

                    <div class="section-title text-center wow fadeInDown">
                        <h2>Contact Us</h2>
                        <p>contact us for detail information.</p>
                    </div>
                    
                    <div class="col-md-12 col-sm-9 wow fadeInLeft">
                        <div class="contact-form clearfix">
                            <form action="actionContact.php" method="post">
                                <div class="input-field">
                                    <input type="text" class="form-control" name="name" placeholder="Your Name" required="">
                                </div>
                                <div class="input-field">
                                    <input type="email" class="form-control" name="email" placeholder="Your Email" required="">
                                </div>
                                <div class="input-field message">
                                    <textarea name="message" class="form-control" placeholder="Your Message" required=""></textarea>
                                </div>
                                <input type="submit" class="btn btn-blue pull-right" value="SEND MESSAGE" id="msg-submit">
                            </form>
                        </div> <!-- end .contact-form -->
                    </div> <!-- .col-md-8 -->

                   

                </div>
            </div>
        </section>
        <!--
        End #contact
        ========================== -->

        <!--
        #footer
        ========================== -->
        <footer id="footer" class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="footer-logo wow fadeInDown">
                            <img src="img/logo_100.png" alt="logo">
                        </div>

                        <div class="footer-social wow fadeInUp">
                            <h3>We are social</h3>
                            <ul class="text-center list-inline">
                                <li><a href="https://www.facebook.com/kontritech"><i class="fa fa-facebook fa-lg"></i></a></li>
                                <li><a href="https://www.instagram.com/kontritech_/"><i class="fa fa-instagram fa-lg"></i></a></li>
                                <!-- <li><a href="http://goo.gl/r4xzR4"><i class="fa fa-google-plus fa-lg"></i></a></li>
                                <li><a href="http://goo.gl/k9zAy5"><i class="fa fa-dribbble fa-lg"></i></a></li> -->
                            </ul>
                        </div>

                        <div class="copyright">
                           
                            <p>Theme by <a href="http://graphberry.com">GraphBerry.com</a> Edited by <a target="_blank" href="http://kontritech.com">Kontritech</a></p>
                        </div>

                    </div>
                </div>
            </div>
        </footer>
        <!--
        End #footer
        ========================== -->


        <!--
        JavaScripts
        ========================== -->
        
        <!-- main jQuery -->
        <script src="js/vendor/jquery-1.11.1.min.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
        <!-- jquery.nav -->
        <script src="js/jquery.nav.js"></script>
        <!-- Portfolio Filtering -->
        <script src="js/jquery.mixitup.min.js"></script>
        <!-- Fancybox -->
        <script src="js/jquery.fancybox.pack.js"></script>
        <!-- Parallax sections -->
        <script src="js/jquery.parallax-1.1.3.js"></script>
        <!-- jQuery Appear -->
        <script src="js/jquery.appear.js"></script>
        <!-- countTo -->
        <script src="js/jquery-countTo.js"></script>
        <!-- owl carousel -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- WOW script -->
        <script src="js/wow.min.js"></script>
        <!-- theme custom scripts -->
        <script src="js/main.js"></script>
    </body>
</html>
